ci-images
=========

This repository contains docker images that will be used by lava ci jobs.

The CI job for this repository is responsible for building, testing and
publishing the Docker images on hub.lavasoftware.org/lava/ci-images/

The [registry](https://git.lavasoftware.org/lava/ci-images/container_registry)
list the available images.

You can manually build and test the images, by running `./build.sh`
